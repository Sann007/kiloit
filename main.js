let form = document.getElementById("form");
let NameInput = document.getElementById("NameInput");
let QtyInput = document.getElementById("QtyInput");
let PriceInput = document.getElementById("PriceInput");
let msg = document.getElementById("msg");
let product = document.querySelector(".product");
let add = document.getElementById("add");

// Change add button into add submit button
form.addEventListener("submit", (e) => {
  e.preventDefault();
  formValidation();
});

// Name product box can not be blank
let formValidation = () => {
  if (NameInput.value === "") {
    console.log("failure");
    msg.innerHTML = "Name cannot be blank";
  } else {
    console.log("success");
    msg.innerHTML = "";
    acceptData();
    add.setAttribute("data-bs-dismiss", "modal");
    add.click();
  }
};

// add product

let data = {};

let acceptData = () => {
  data["name"] = NameInput.value;
  data["qty"] = QtyInput.value;
  data["price"] = PriceInput.value;
  localStorage.setItem("productData", JSON.stringify(data));
  console.log(data);
  createproduct();
};
let createproduct = () => {
  let storedData = JSON.parse(localStorage.getItem("productData"));

  // if (storedData) {
  if (storedData !== null) {
    let html = `
    
      <div class="product1">
        <span class="fw-bold">${storedData.name}</span>
        <span>Qty:${storedData.qty}</span>
        <span>Price:${storedData.price}</span>

        <span class="options">
          <i onclick="editproduct(this)" data-bs-toggle="modal" data-bs-target="form" class="fas fa-edit"></i>
          <i onclick="deleteproduct(this)" class="fa fa-trash" aria-hidden="true"></i>
        </span>
      </div>
    
    `;
    resetform();
    product.innerHTML += html;
  } else {
    product.innerHTML = "No product data found";
  }
};

// delete
let deleteproduct = (e) => {
  e.parentElement.parentElement.remove();
};

// edit
let editproduct = (e) => {
  let selectedproduct = e.parentElement.parentElement;
  NameInput.value = selectedproduct.children[0].innerHTML;
  QtyInput.value = selectedproduct.children[1].innerHTML;
  PriceInput.value = selectedproduct.children[2].innerHTML;

  selectedproduct.remove();
};

// reset
let resetform = () => {
  NameInput.value = "";
  QtyInput.value = "";
  PriceInput.value = "";
};



// search bar 


function searchProducts() {
  var input, filter, products, i, j, productName, productDetails, txtValue;
  input = document.getElementById('searchInput');
  filter = input.value.toUpperCase();

  
  products = document.getElementsByClassName('product');

  for (i = 0; i < products.length; i++) {
    var product1 = products[i].getElementsByClassName('product1')[0]
    productName = product1.querySelector('.fw-bold');
    productDetails = product1.querySelectorAll('span:not(.fw-bold)');
    txtValue = productName.textContent || productName.innerText;

    if (txtValue.toUpperCase().indexOf(filter) > -1) {
      products[i].style.display = '';
      for (j = 0; j < productDetails.length; j++) {
        productDetails[j].style.display = '';
      }
    } else {
      products[i].style.display = 'none';
      for (j = 0; j < productDetails.length; j++) {
        productDetails[j].style.display = 'none';
      }
    }
  }
}
